//  UserStorage.swift
//  TableView
//  Created by Viktoriia Skvarko


import Foundation
import UIKit

struct Contact {
    var firstName: String
    var lastName: String
    var phone: String
    var country: String
    var city: String
    var group: String
    
    func description() -> String {
        let descr = firstName + " " + lastName + " " + phone
        return descr
    }
}

