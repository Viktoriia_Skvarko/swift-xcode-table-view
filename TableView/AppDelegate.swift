//  AppDelegate.swift
//  TableView
//  Created by Viktoriia Skvarko


import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var contact = Contact(firstName: "", lastName: "", phone: "", country: "", city: "", group: "")
    var contactList = ContactList(myContacts: [])
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

