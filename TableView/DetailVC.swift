//  DetailVC.swift
//  TableView
//  Created by Viktoriia Skvarko


import UIKit
import Foundation

class DetailVC: UIViewController {
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var lastNameLable: UILabel!
    @IBOutlet weak var phoneLable: UILabel!
    
    @IBOutlet weak var countryLable: UILabel!
    @IBOutlet weak var cityLable: UILabel!
    @IBOutlet weak var groupeLable: UILabel!
    
    @IBAction func okDetaliAction(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    
    var contactDetail: Contact?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLable.text = contactDetail?.firstName
        lastNameLable.text = contactDetail?.lastName
        phoneLable.text = contactDetail?.phone
        countryLable.text = contactDetail?.country
        cityLable.text = contactDetail?.city
        groupeLable.text = contactDetail?.group
        
    }
}

