//  AddContactVC.swift
//  TableView
//  Created by Viktoriia Skvarko


import UIKit
import Foundation

class AddContactVC: UIViewController {
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var groupe: UITextField!
    
    
    @IBAction func addContactAction(_ sender: UIButton) {
        
        appDelegate.contact.firstName = firstName.text!
        appDelegate.contact.lastName = lastName.text!
        appDelegate.contact.phone = phone.text!
        appDelegate.contact.country = country.text!
        appDelegate.contact.city = city.text!
        appDelegate.contact.group = groupe.text!
        
        let oneContact = Contact(firstName: appDelegate.contact.firstName, lastName: appDelegate.contact.lastName, phone: appDelegate.contact.phone, country: appDelegate.contact.country, city: appDelegate.contact.city, group: appDelegate.contact.group)
        
        appDelegate.contactList.myContacts.append(oneContact)
        
        dismiss(animated: true)
    }
}
