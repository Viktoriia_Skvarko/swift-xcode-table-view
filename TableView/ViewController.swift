//  ViewController.swift
//  TableView
//  Created by Viktoriia Skvarko


import UIKit
import Foundation

class ViewController: UIViewController {
    
    
    @IBOutlet weak var tableOutlet: UITableView!
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableOutlet.reloadData()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard segue.identifier == "detailSegue" else { return }
        
        if let cell = sender as? UITableViewCell, let indexPath = tableOutlet.indexPath(for: cell), let vc = segue.destination as? DetailVC {
            vc.contactDetail = appDelegate.contactList.myContacts[indexPath.row]
        }
    }
    
}


extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let counts = appDelegate.contactList.myContacts.count
        return counts
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell Inform", for: indexPath)
        cell.textLabel?.text = appDelegate.contactList.myContacts[indexPath.row].description()
        return cell
    }
}

